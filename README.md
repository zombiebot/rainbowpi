# rainbowpi
Minetest mod

## How to use
Use item ```rainbowpi:colorful_bucket``` (this item available only in creative inventory or via ```give``` commands).

## License
MIT

## Media files
* rainbowpi_bucket.png - made based on bucked image from bucked mod by ElementW (CC BY-SA 3.0)
* rainbowpi_c\*.png - by xeranas CC-0
* rainbowpi_stream.ogg - by Yoyodaman234 CC-0 from [freesound.org](https://www.freesound.org/people/Yoyodaman234/sounds/349072/)
